# Assignment

## CI running tests

Gitlab-CI: https://gitlab.com/knela/oop/pipelines

## Run it

```bash
$ sudo docker-compose up
```

It will bring up 2 containers, one for the server running with gunicorn (wsgi) bringing up the service it self, and the other with nginx, serving static files,
and passing requisitions to gunicorn.

You can find the api swagger dashboard running at `localhost:80`.

## CRUDS

All the additions, deletions and editions can be made by posts, patches, deletes at the endpoins of the api. (You can create Students and Teachers by posting their data,
and leaving boletins and schoolclasses empty).

Some of the endpoints of the api require specific types of users logged in. Like
the ones regarding answering quizzes or grading them.
