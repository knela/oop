from rest_framework.serializers import ModelSerializer  # , ValidationError
from app.models import (
    Quiz, Question, Teacher, SchoolClass, Boletim, Student,
)
from django.contrib.auth.password_validation import validate_password
from django.contrib.auth.hashers import make_password
from rest_framework.serializers import ValidationError


class QuestionSerializer(ModelSerializer):
    class Meta:
        model = Question
        fields = ('pk', 'question', 'answer', 'quiz')


class QuizSerializer(ModelSerializer):
    questions = QuestionSerializer(many=True, read_only=True)

    class Meta:
        model = Quiz
        fields = ('pk', 'score', 'is_completed', 'questions', 'student')


class StudentSerializer(ModelSerializer):
    quizzes = QuizSerializer(many=True, read_only=True)

    class Meta:
        model = Student
        fields = ('pk', 'username', 'password',
                  'email', 'first_name', 'last_name', 'schoolclass', 'quizzes')

        extra_kwargs = {
            'password': {'write_only': True},
        }

    def validate_password(self, data):
        try:
            validate_password(password=data)
        except Exception:
            raise ValidationError(
                'Invalid Password.'
            )
        return make_password(data)

    def create(self, validated_data):
        instance = Student(**validated_data)
        instance.save()
        return instance


class SchoolClassSerializer(ModelSerializer):
    students = StudentSerializer(many=True, read_only=True)

    class Meta:
        model = SchoolClass
        fields = ('pk', 'name', 'changed_score', 'teacher', 'students')


class TeacherSerializer(ModelSerializer):
    schoolclasses = SchoolClassSerializer(many=True, read_only=True)

    class Meta:
        model = Teacher
        fields = ('pk', 'username', 'password',
                  'email', 'first_name', 'last_name', 'schoolclasses',
                  'boletim')

        extra_kwargs = {
            'password': {'write_only': True},
        }

    def validate_password(self, data):
        try:
            validate_password(password=data)
        except Exception:
            raise ValidationError(
                'Invalid Password.'
            )
        return make_password(data)

    def create(self, validated_data):
        instance = Teacher(**validated_data)
        instance.save()
        return instance


class BoletimSerializer(ModelSerializer):
    teacher = TeacherSerializer(read_only=True)

    class Meta:
        model = Boletim
        fields = ('pk', 'boletim', 'teacher')
