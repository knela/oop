import json
import logging
from django.db import models
from django.contrib.auth.models import User
from django.core.exceptions import ObjectDoesNotExist
from django.core.exceptions import ValidationError
import time


logging.basicConfig(filename='app.log', level=logging.DEBUG)


class ModelManager(models.Manager):
    """
    Model manager to prevent inheritance to breack for Non existing models
    """
    use_for_related_fields = True

    def get_or_none(self, **kwargs):
        try:
            return self.get(**kwargs)
        except self.model.DoesNotExist:
            return None


class UserModel(models.Model):
    """
    User model to use dajngo's user Model as an abstract class
    """
    objects = ModelManager()

    class Meta:
        abstract = True


class Quiz(models.Model):
    """
    Quiz model
    """
    score = models.PositiveSmallIntegerField(default=0, null=True)
    is_completed = models.NullBooleanField()

    student = models.ForeignKey(
        'Student', related_name='quizzes',
        on_delete=models.CASCADE, null=True
    )


class Question(models.Model):
    """
    Qustion model
    """
    question = models.CharField(max_length=200)
    A = 'A'
    B = 'B'
    C = 'C'
    Anser_CHOICES = (
        (A, 'A'),
        (B, 'B'),
        (C, 'C'),
    )
    answer = models.CharField(
        max_length=2,
        choices=Anser_CHOICES,
        null=True,
    )
    quiz = models.ForeignKey(
        Quiz, related_name='questions',
        on_delete=models.CASCADE, null=True
    )


class Teacher(User, UserModel):
    """
    Teacher model. A teacher can create and grade quizzes. Also, show
    its students scores.
    """
    boletim = models.OneToOneField(
        'Boletim', related_name='teacher',
        on_delete=models.DO_NOTHING,
        null=True
    )

    def name(self):
        return '{} {}'.format(self.first_name, self.last_name)

    @classmethod
    def create_quiz(cls, student, questions):
        quiz = Quiz()
        quiz.student = student
        quiz.save()
        for question_str in questions:
            if not isinstance(question_str, str):
                raise ValidationError(
                    'Questions need to be strings'
                )
            question = Question()
            question.question = question_str
            question.quiz = quiz
            question.save()

    def grade_quiz(self, quiz, correct_answers):
        count = 0
        temp_score = 0
        final_score = 0
        if not quiz.is_completed:
            raise ValidationError(
                'Quiz not completed. Cannot grade not completed quizzes'
            )
        for correct_answer, question in zip(
            correct_answers, quiz.questions.all()
        ):
            count += 1
            if question.answer == correct_answer:
                temp_score += 1
        final_score = int(temp_score/count * 100)
        quiz.score = final_score
        quiz.student.schoolclass.changed_score = True
        quiz.student.schoolclass.save()
        quiz.student.save()
        quiz.save()
        self.save()
        return final_score

    def calc_total_student_score(self):
        """
        Calculates all the total scores for all students of this teacher
        schollclasses. This method would always be O(n*m*k) because of
        the nested loops. A workarround was to make it store the scores, and
        only iterate again if it changes. Its worse case is O(n^3) still, but
        its linear if nothing is changed.
        """
        start_time = time.time()
        schoolclasses = self.schoolclasses.all()
        response = {}

        for schoolclass in schoolclasses:
            if not schoolclass.changed_score:
                logging.info("using stored data")
                boletim_stored = self.boletim.get_boletim()
                response[schoolclass.name] = boletim_stored[schoolclass.name]
            else:
                logging.info("data has been changed, getting new data")
                schoolclass.changed_score = False
                schoolclass.save()
                response[schoolclass.name] = {}
                for student in schoolclass.students.all():
                    user_full_name = "{} {}".format(
                        student.first_name, student.last_name
                    )
                    response[schoolclass.name][user_full_name] = {}
                    response[
                        schoolclass.name
                    ][
                        user_full_name
                    ][
                        "semester_scr"
                    ] = 0
                    total = 0
                    for idx, quiz in enumerate(student.quizzes.all()):
                        response[
                            schoolclass.name
                        ][
                            user_full_name
                        ][
                            str(idx + 1)
                        ] = quiz.score
                        total += quiz.score
                    response[
                        schoolclass.name
                    ][
                        user_full_name
                    ][
                        "semester_scr"
                    ] = total
        boletim = Boletim()
        boletim.set_boletim(response)
        boletim.save()
        self.boletim = boletim
        self.boletim.save()
        self.save()
        logging.info("-- {} seconds --".format(str(time.time() - start_time)))
        return response


class SchoolClass(models.Model):
    """
    SchoolClass model
    """
    name = models.CharField(max_length=200)
    changed_score = models.NullBooleanField()
    teacher = models.ForeignKey(
        Teacher, related_name='schoolclasses',
        on_delete=models.DO_NOTHING, null=True
    )


class Boletim(models.Model):
    """
    Boletim model. Stores total scores for students
    """
    boletim = models.CharField(max_length=200)

    def set_boletim(self, x):
        self.boletim = json.dumps(x)

    def get_boletim(self):
        return json.loads(self.boletim)


class Student(User, UserModel):
    """
    Student model. They can answer quizzes. They can send null (None) to
    answers that dont want do answer yet. A quiz is only completed if it does
    not have None fields.
    """
    schoolclass = models.ForeignKey(
        SchoolClass, related_name='students',
        on_delete=models.DO_NOTHING, null=True
    )

    def answer_quiz(self, quiz, answers):
        if quiz not in self.quizzes.all():
            raise ObjectDoesNotExist(
                'This quiz is not assigned to this student'
            )
        if None not in answers:
            quiz.is_completed = True
        for answer, question in zip(answers, quiz.questions.all()):
            if answer not in ['A', 'B', 'C', None]:
                raise ValidationError('Answer needs to be "A" or "B" or "C"')
            question.answer = answer
            if not answer:
                quiz.is_completed = False
            question.save()
        if quiz.is_completed is None:
            quiz.is_completed = True
        quiz.save()
        self.save()
        response = {}

        response[
            quiz.questions.all()[0].question
        ] = quiz.questions.all()[0].answer
        response[
            quiz.questions.all()[1].question
        ] = quiz.questions.all()[1].answer
        return response

    def name(self):
        return '{} {}'.format(self.first_name, self.last_name)
