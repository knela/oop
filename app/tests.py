# from django.core.exceptions import ObjectDoesNotExist
from django.core.exceptions import ValidationError
from app.models import (
    Quiz, Question, Teacher, SchoolClass, Boletim, Student,
)
import time
from rest_framework import status
from rest_framework.test import APITestCase


class SchoolClassTestCase(APITestCase):

    def setUp(self):
        self.schoolclass = SchoolClass()
        self.schoolclass.name = '14b'

        self.teacher = Teacher()
        self.teacher.username = 'teacherusername'
        self.teacher.email = 'email@email.com'
        self.teacher.set_password('123546')
        self.teacher.save()

        self.schoolclass.teacher = self.teacher
        self.schoolclass.save()

        self.student = Student()
        self.student.username = 'studentusername'
        self.student.first_name = 'John'
        self.student.last_name = 'Doe'
        self.student.email = 'email@email.com'
        self.student.set_password('123546')
        self.student.schoolclass = self.schoolclass
        self.student.save()

    def tearDown(self):
        self.student.delete()
        self.schoolclass.delete()
        self.teacher.delete()

    def test_quiz_instance(self):
        quiz = Quiz()
        self.assertTrue(quiz)

    def test_question_instance(self):
        question = Question()
        self.assertTrue(question)

    def test_teacher_instance(self):
        teacher = Teacher()
        self.assertTrue(teacher)

    def test_create_quiz(self):
        db_before = len(Quiz.objects.all())
        Teacher.create_quiz(self.student, ["question 1", "question 2"])
        db_after = len(Quiz.objects.all())
        q1 = Question.objects.get(question='question 1')
        q2 = Question.objects.get(question='question 2')
        qz = Quiz.objects.all()[0]
        qz.delete()
        q1.delete()
        q2.delete()
        self.assertTrue(q1)
        self.assertTrue(q2)
        self.assertGreater(db_after, db_before)

    def test_create_quiz_no_string_questions(self):
        with self.assertRaises(ValidationError):
            Teacher.create_quiz(self.student, [1, None])

    def test_grade_quiz_valid_all_correct(self):
        self.teacher.create_quiz(self.student, ["question 1", "question 2"])
        quiz = Quiz.objects.all()[0]
        self.student.answer_quiz(quiz, ['A', 'B'])
        self.teacher.grade_quiz(quiz, ['A', 'B'])
        quiz = Quiz.objects.all()[0]
        self.assertEqual(100, quiz.score)
        self.assertTrue(quiz.student.schoolclass.changed_score)
        quiz.delete()

    def test_grade_quiz_valid_all_incorrect(self):
        self.teacher.create_quiz(self.student, ["question 1", "question 2"])
        quiz = Quiz.objects.all()[0]
        self.student.answer_quiz(quiz, ['A', 'B'])
        self.teacher.grade_quiz(quiz, ['C', 'C'])
        quiz = Quiz.objects.all()[0]
        self.assertEqual(0, quiz.score)
        self.assertTrue(quiz.student.schoolclass.changed_score)
        quiz.delete()

    def test_grade_quiz_valid_half_incorrect(self):
        self.teacher.create_quiz(self.student, ["question 1", "question 2"])
        quiz = Quiz.objects.all()[0]
        self.student.answer_quiz(quiz, ['A', 'B'])
        self.teacher.grade_quiz(quiz, ['A', 'C'])
        quiz = Quiz.objects.all()[0]
        self.assertEqual(50, quiz.score)
        self.assertTrue(quiz.student.schoolclass.changed_score)
        quiz.delete()

    def test_grade_quiz_quiz_not_completed(self):
        with self.assertRaises(ValidationError):
            self.teacher.create_quiz(
                self.student, ["question 1", "question 2"]
            )
            quiz = Quiz.objects.all()[0]
            self.student.answer_quiz(quiz, ['A', None])
            self.teacher.grade_quiz(quiz, ['A', 'B'])
            quiz.delete()

    def test_calc_total_student_score_all_correct(self):
        self.teacher.create_quiz(self.student, ["question 1", "question 2"])
        quiz = Quiz.objects.all()[0]
        self.student.answer_quiz(quiz, ['A', 'B'])
        self.teacher.grade_quiz(quiz, ['A', 'B'])

        self.teacher.create_quiz(self.student, ["question 1", "question 2"])
        quiz1 = Quiz.objects.all()[1]
        self.student.answer_quiz(quiz1, ['B', 'B'])
        self.teacher.grade_quiz(quiz1, ['B', 'B'])

        self.teacher.create_quiz(self.student, ["question 1", "question 2"])
        quiz2 = Quiz.objects.all()[2]
        self.student.answer_quiz(quiz2, ['C', 'B'])
        self.teacher.grade_quiz(quiz2, ['C', 'B'])
        response = self.teacher.calc_total_student_score()
        expected_response = {
            '14b': {
                'John Doe': {
                    'semester_scr': 300,
                    '1': 100,
                    '2': 100,
                    '3': 100
                }
            }
        }
        self.assertEqual(expected_response, response)
        self.assertEqual(self.teacher.boletim.get_boletim(), response)

    def test_calc_total_student_score_partially_correct(self):
        self.teacher.create_quiz(self.student, ["question 1", "question 2"])
        quiz = Quiz.objects.all()[0]
        self.student.answer_quiz(quiz, ['A', 'B'])
        self.teacher.grade_quiz(quiz, ['A', 'B'])

        self.teacher.create_quiz(self.student, ["question 1", "question 2"])
        quiz1 = Quiz.objects.all()[1]
        self.student.answer_quiz(quiz1, ['B', 'B'])
        self.teacher.grade_quiz(quiz1, ['B', 'C'])

        self.teacher.create_quiz(self.student, ["question 1", "question 2"])
        quiz2 = Quiz.objects.all()[2]
        self.student.answer_quiz(quiz2, ['C', 'B'])
        self.teacher.grade_quiz(quiz2, ['C', 'A'])
        response = self.teacher.calc_total_student_score()
        expected_response = {
            '14b': {
                'John Doe': {
                    'semester_scr': 200,
                    '1': 100,
                    '2': 50,
                    '3': 50
                }
            }
        }
        self.assertEqual(expected_response, response)
        self.assertEqual(self.teacher.boletim.get_boletim(), response)

    def test_calc_total_student_score_all_incorrect(self):
        self.teacher.create_quiz(self.student, ["question 1", "question 2"])
        quiz = Quiz.objects.all()[0]
        self.student.answer_quiz(quiz, ['A', 'B'])
        self.teacher.grade_quiz(quiz, ['C', 'A'])

        self.teacher.create_quiz(self.student, ["question 1", "question 2"])
        quiz1 = Quiz.objects.all()[1]
        self.student.answer_quiz(quiz1, ['B', 'B'])
        self.teacher.grade_quiz(quiz1, ['A', 'C'])

        self.teacher.create_quiz(self.student, ["question 1", "question 2"])
        quiz2 = Quiz.objects.all()[2]
        self.student.answer_quiz(quiz2, ['C', 'B'])
        self.teacher.grade_quiz(quiz2, ['A', 'C'])
        response = self.teacher.calc_total_student_score()
        expected_response = {
            '14b': {
                'John Doe': {
                    'semester_scr': 0,
                    '1': 0,
                    '2': 0,
                    '3': 0
                }
            }
        }
        self.assertEqual(expected_response, response)
        self.assertEqual(self.teacher.boletim.get_boletim(), response)

    def test_calc_total_student_score_some_invalid(self):
        with self.assertRaises(ValidationError):
            self.teacher.create_quiz(
                self.student, ["question 1", "question 2"]
            )
            quiz = Quiz.objects.all()[0]
            self.student.answer_quiz(quiz, ['invalid', 'B'])
            self.teacher.grade_quiz(quiz, ['A', 'A'])

            self.teacher.create_quiz(
                self.student, ["question 1", "question 2"]
            )
            quiz1 = Quiz.objects.all()[1]
            self.student.answer_quiz(quiz1, ['B', 'B'])
            self.teacher.grade_quiz(quiz1, ['A', 'C'])

            self.teacher.create_quiz(
                self.student, ["question 1", "question 2"]
            )
            quiz2 = Quiz.objects.all()[2]
            self.student.answer_quiz(quiz2, ['C', 'invalid'])
            self.teacher.grade_quiz(quiz2, ['A', 'C'])

    def test_calc_total_student_score_faster_for_unchanged(self):
        self.teacher.create_quiz(self.student, ["question 1", "question 2"])
        quiz = Quiz.objects.all()[0]
        self.student.answer_quiz(quiz, ['A', 'B'])
        self.teacher.grade_quiz(quiz, ['C', 'A'])

        self.teacher.create_quiz(self.student, ["question 1", "question 2"])
        quiz1 = Quiz.objects.all()[1]
        self.student.answer_quiz(quiz1, ['B', 'B'])
        self.teacher.grade_quiz(quiz1, ['A', 'C'])

        self.teacher.create_quiz(self.student, ["question 1", "question 2"])
        quiz2 = Quiz.objects.all()[2]
        self.student.answer_quiz(quiz2, ['C', 'B'])
        self.teacher.grade_quiz(quiz2, ['A', 'C'])
        start_time = time.time()
        response = self.teacher.calc_total_student_score()
        changed_data_time = time.time() - start_time
        start_time = time.time()
        response_unchanged = self.teacher.calc_total_student_score()
        unchanged_data_time = time.time() - start_time
        expected_response = {
            '14b': {
                'John Doe': {
                    'semester_scr': 0,
                    '1': 0,
                    '2': 0,
                    '3': 0
                }
            }
        }
        self.assertEqual(expected_response, response)
        self.assertEqual(self.teacher.boletim.get_boletim(), response)
        self.assertEqual(response, response_unchanged)
        self.assertLess(unchanged_data_time, changed_data_time)

    def test_schoolclasses_instance(self):
        schoolclasses = SchoolClass()
        self.assertTrue(schoolclasses)

    def test_boletim_instance(self):
        boletim = Boletim()
        self.assertTrue(boletim)

    def test_student_instance(self):
        student = Student()
        self.assertTrue(student)

    def test_answer_quiz_valid_answer(self):
        self.teacher.create_quiz(
            self.student, ["question 1", "question 2"]
        )
        quiz = Quiz.objects.all()[0]
        self.student.answer_quiz(quiz, ['A', 'B'])
        first_quiz_from_user = self.student.quizzes.all()[0]
        first_question_from_user = first_quiz_from_user.questions.all()[0]
        second_question_from_user = first_quiz_from_user.questions.all()[1]
        self.assertEqual('A', first_question_from_user.answer)
        self.assertEqual('B', second_question_from_user.answer)
        first_quiz_from_user.delete()
        first_question_from_user.delete()
        second_question_from_user.delete()

    def test_answer_quiz_invalid_answer(self):
        with self.assertRaises(ValidationError):
            self.teacher.create_quiz(
                self.student, ["question 1", "question 2"]
            )
            quiz = Quiz.objects.all()[0]
            self.student.answer_quiz(quiz, ['invalid', 'invalid'])

    def test_answer_quiz_some_null_values(self):
        self.teacher.create_quiz(
            self.student, ["question 1", "question 2"]
        )
        quiz = Quiz.objects.all()[0]
        self.student.answer_quiz(quiz, ['A', None])
        first_quiz_from_user = self.student.quizzes.all()[0]
        first_question_from_user = first_quiz_from_user.questions.all()[0]
        second_question_from_user = first_quiz_from_user.questions.all()[1]
        self.assertEqual('A', first_question_from_user.answer)
        self.assertEqual(None, second_question_from_user.answer)
        first_quiz_from_user.delete()
        first_question_from_user.delete()
        second_question_from_user.delete()

# Tests for views

    def test_solve_quiz_endpoint(self):
        self.teacher.create_quiz(
            self.student, ["question 1", "question 2"]
        )
        quiz = Quiz.objects.all()[0]
        data = {}
        data["quiz"] = '1'
        data["answers"] = ['A', 'B']
        self.client.force_login(self.student)
        response = self.client.post(
            "/api/students/solve_quiz/",
            data,
            format='json'
        )
        self.assertEqual(status.HTTP_200_OK, response.status_code)
        quiz.delete()

    def test_solve_quiz_invalid_answers_endpoint(self):
        self.teacher.create_quiz(
            self.student, ["question 1", "question 2"]
        )
        quiz = Quiz.objects.all()[0]
        data = {}
        data["quiz"] = '1'
        data["answers"] = ['A', 'invalid']
        self.client.force_login(self.student)
        response = self.client.post(
            "/api/students/solve_quiz/",
            data,
            format='json'
        )
        self.assertEqual(status.HTTP_400_BAD_REQUEST, response.status_code)
        quiz.delete()

    def test_grade_quiz_endpoint(self):
        self.teacher.create_quiz(
            self.student, ["question 1", "question 2"]
        )
        quiz = Quiz.objects.all()[0]
        self.student.answer_quiz(quiz, ['A', 'B'])
        data = {}
        data["quiz"] = '1'
        data["correct_answers"] = ['A', 'B']
        self.client.force_login(self.teacher)
        response = self.client.post(
            "/api/teachers/grade_quiz/",
            data,
            format='json'
        )
        self.assertEqual(status.HTTP_200_OK, response.status_code)
        quiz.delete()

    def test_grade_uncompleted_quiz_endpoint(self):
        self.teacher.create_quiz(
            self.student, ["question 1", "question 2"]
        )
        quiz = Quiz.objects.all()[0]
        self.student.answer_quiz(quiz, ['A', None])
        data = {}
        data["quiz"] = '1'
        data["correct_answers"] = ['A', 'B']
        self.client.force_login(self.teacher)
        response = self.client.post(
            "/api/teachers/grade_quiz/",
            data,
            format='json'
        )
        self.assertEqual(status.HTTP_400_BAD_REQUEST, response.status_code)
        quiz.delete()

    def test_calc_scores_endpoint(self):
        self.teacher.create_quiz(
            self.student, ["question 1", "question 2"]
        )
        quiz = Quiz.objects.all()[0]
        self.student.answer_quiz(quiz, ['A', 'B'])
        self.teacher.grade_quiz(quiz, ['A', 'B'])
        data = {}
        data["quiz"] = '1'
        data["correct_answers"] = ['A', 'B']
        self.client.force_login(self.teacher)
        response = self.client.get(
            "/api/teachers/calc_scores/",
            data,
            format='json'
        )
        self.assertEqual(status.HTTP_200_OK, response.status_code)
        quiz.delete()

    def test_calc_scores_no_quiz_endpoint(self):
        self.teacher.create_quiz(
            self.student, ["question 1", "question 2"]
        )
        quiz = Quiz.objects.all()[0]
        data = {}
        data["quiz"] = '1'
        data["correct_answers"] = ['A', 'B']
        self.client.force_login(self.teacher)
        response = self.client.get(
            "/api/teachers/calc_scores/",
            data,
            format='json'
        )
        self.assertEqual(status.HTTP_400_BAD_REQUEST, response.status_code)
        quiz.delete()

    def test_calc_scores_quiz_not_graded_endpoint(self):
        self.teacher.create_quiz(
            self.student, ["question 1", "question 2"]
        )
        quiz = Quiz.objects.all()[0]
        self.student.answer_quiz(quiz, ['A', 'B'])
        data = {}
        data["quiz"] = '1'
        data["correct_answers"] = ['A', 'B']
        self.client.force_login(self.teacher)
        response = self.client.get(
            "/api/teachers/calc_scores/",
            data,
            format='json'
        )
        self.assertEqual(status.HTTP_400_BAD_REQUEST, response.status_code)
        quiz.delete()
