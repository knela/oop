# from django.shortcuts import render
# from django.http import HttpResponse
from rest_framework import viewsets
from app.serializers import (
    QuestionSerializer, QuizSerializer, SchoolClassSerializer,
    TeacherSerializer, BoletimSerializer, StudentSerializer
)
from app.models import (
    Quiz, Question, Teacher, SchoolClass, Boletim, Student,
)
from rest_framework_swagger.views import get_swagger_view
from rest_framework.decorators import list_route
from rest_framework.response import Response
from django.core.exceptions import ObjectDoesNotExist
from django.core.exceptions import ValidationError
from rest_framework import status

schema_view = get_swagger_view(title='School Classes API')


class QuestionViewSet(viewsets.ModelViewSet):
    """
    """
    queryset = Question.objects.all()
    serializer_class = QuestionSerializer


class QuizViewSet(viewsets.ModelViewSet):
    """
    """
    queryset = Quiz.objects.all()
    serializer_class = QuizSerializer


class SchoolClassViewSet(viewsets.ModelViewSet):
    """
    """
    queryset = SchoolClass.objects.all()
    serializer_class = SchoolClassSerializer


class TeacherViewSet(viewsets.ModelViewSet):
    """
    """
    queryset = Teacher.objects.all()
    serializer_class = TeacherSerializer

    @list_route(methods=['get'])
    def calc_scores(self, request):
        teacher = request.user.teacher
        try:
            response = teacher.calc_total_student_score()
            status_code = status.HTTP_200_OK
        except ObjectDoesNotExist:
            response = {"Students, Teachers and schoolclasses need to exist"}
            status_code = status.HTTP_400_BAD_REQUEST
        except AttributeError:
            response = {"Quizzes need to be grade to give scores"}
            status_code = status.HTTP_400_BAD_REQUEST

        return Response(response, status=status_code)

    @list_route(methods=['post'])
    def grade_quiz(self, request):
        """
        API endpoint that allows teachers to grade a quiz (a teacher needs
        to be logged in). Please ignore swagger example.
        ---
        Body example:
        ```
        {
          "quiz": "1",
          "correct_answers": ["A", "B"]
        }
        ```
        Response example:
        ```
        {
          "score": "100"
        }
        ```
        """
        quiz_pk = request.data['quiz']
        correct_answers = request.data['correct_answers']
        teacher = request.user.teacher
        quiz = Quiz.objects.get(pk=quiz_pk)
        try:
            response = {}
            response["score"] = teacher.grade_quiz(quiz, correct_answers)
            status_code = status.HTTP_200_OK
        except ValidationError:
            response = {}
            response['quiz'] = ["Quiz not completed. Cannot grade not\
                    completed quizzes"]
            status_code = status.HTTP_400_BAD_REQUEST

        return Response(response, status=status_code)


class BoletimViewSet(viewsets.ModelViewSet):
    """
    """
    queryset = Boletim.objects.all()
    serializer_class = BoletimSerializer


class StudentViewSet(viewsets.ModelViewSet):
    queryset = Student.objects.all()
    serializer_class = StudentSerializer

    @list_route(methods=['post'])
    def solve_quiz(self, request):
        """
        API endpoint that allows students to answer a quiz (a student needs
        to be logged in). Please ignore swagger example.
        ---
        Body example:
        ```
        {
          "quiz": "1",
          "answers": ["A", null]
        }
        ```
        Response example:
        ```
        {
          "question 1?": "A",
          "question 2?": null
        }
        ```
        """
        quiz_pk = request.data['quiz']
        answers = request.data['answers']
        student = request.user.student
        quiz = Quiz.objects.get(pk=quiz_pk)
        try:
            response = student.answer_quiz(quiz, answers)
            status_code = status.HTTP_200_OK
        except ValidationError:
            response = {}
            response['answers'] = ["Invalid answers"]
            status_code = status.HTTP_400_BAD_REQUEST

        return Response(response, status=status_code)
