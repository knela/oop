# from django.urls import path
from django.conf.urls import include, url
from rest_framework import routers
from app import views
from app.views import schema_view


router = routers.DefaultRouter()
router.register(r'questions', views.QuestionViewSet)
router.register(r'quizzes', views.QuizViewSet)
router.register(r'schoolclasses', views.SchoolClassViewSet)
router.register(r'teachers', views.TeacherViewSet)
router.register(r'boletins', views.BoletimViewSet)
router.register(r'students', views.StudentViewSet)


urlpatterns = [
    url(r'^api/', include(router.urls)),
    url(r'^api/', include(router.urls)),
    url(r'^api/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^$', schema_view),
    # path('', views.index, name='index'),
]
