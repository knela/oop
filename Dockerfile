FROM python:3
 RUN mkdir /code
 WORKDIR /code
 ADD . /code/
 RUN pip install -r /code/requirements.txt
 RUN python3 /code/manage.py makemigrations
 RUN python3 /code/manage.py migrate
 # RUN python3 /code/manage.py collectstatic

COPY logging.conf /logging.conf
COPY gunicorn.conf /gunicorn.conf

EXPOSE 8000

ENTRYPOINT ["/usr/local/bin/gunicorn", "--config", "/gunicorn.conf", "--log-config", "/logging.conf", "-b", ":8000", "class.wsgi"]
